package com.bakikocak.bedrockchallenge.ui.mainscreen

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.espresso.contrib.RecyclerViewActions.*
import com.bakikocak.bedrockchallenge.R
import com.bakikocak.bedrockchallenge.adapter.ItemRecyclerViewAdapter
import com.bakikocak.bedrockchallenge.utils.EspressoIdlingResource
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.core.Is
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @get: Rule
    val activityRule : ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)


    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    /**
     *  Recyclerview comes into view
     * */

    @Test
    fun test_isRecyclerviewVisible_onAppLaunch() {
        onView(withId(R.id.item_list)).check(matches(isDisplayed()))

        onView(withId(R.id.item_list)).check( RecyclerViewItemCountAssertion(52));
    }

    /**
     *  Select a item then navigates to second screen
     * */

    @Test
    fun test_isItemNameVisible_onRecyclerViewItemClick() {
        onView(withId(R.id.item_list)).perform(actionOnItemAtPosition<ItemRecyclerViewAdapter.ViewHolder>(0, click()))
        onView(withId(R.id.item_detail_name)).check(matches(isDisplayed()))
    }

    /**
     *  select a item then navigates to second screen
     *  pressing back gets to the first screen
     * */

    @Test
    fun test_backNavigation_toMainScreen() {

        onView(withId(R.id.item_list)).perform(actionOnItemAtPosition<ItemRecyclerViewAdapter.ViewHolder>(0, click()))
        onView(withId(R.id.item_detail_name)).check(matches(isDisplayed()))

        pressBack()
        onView(withId(R.id.item_list)).check(matches(isDisplayed()))
    }


    class RecyclerViewItemCountAssertion(i: Int) : ViewAssertion {
        var expectedCountNumber: Int = i

        override fun check(view: View?, noViewFoundException: NoMatchingViewException?) {
            if (noViewFoundException != null) throw noViewFoundException

            var recyclerView: RecyclerView = view as RecyclerView

            assertThat(recyclerView.adapter?.itemCount, Is(equalTo(expectedCountNumber)))
        }

    }
}
