package com.bakikocak.bedrockchallenge.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.bakikocak.bedrockchallenge.model.Item
import com.bakikocak.bedrockchallenge.model.ItemDao
import com.bakikocak.bedrockchallenge.network.ItemApi
import com.bakikocak.bedrockchallenge.utils.EspressoIdlingResource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ItemViewModel(
    private val itemDao: ItemDao,
    private val itemName: String
) : BaseViewModel() {
    // The instance of Item Api that will be injected by Dagger for retrieving data.
    @Inject
    lateinit var itemApi: ItemApi

    private lateinit var subscription: Disposable

    // will be observed by MainActivity
    var itemData: MutableLiveData<Item> = MutableLiveData()
    var listOfItems: MutableLiveData<List<Item>> = MutableLiveData()
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()

    val errorClickListener = View.OnClickListener { getSingleItem(itemName) }

    private val itemNameLiveData = MutableLiveData<String>()
    private val itemTextLiveData = MutableLiveData<String>()
    private val itemImageUrlLiveData = MutableLiveData<String>()

    init {
        getSingleItem(itemName)
        getItems()
    }

    private fun getSingleItem(name: String) {

        /**
         *  Note that getSingleItem data is not saved in room database for demonstration reasons
         *  Please see readme file about error handling on ViewModels.
         * */

        EspressoIdlingResource.increment() // For testing purposes

        subscription = itemApi
            .getSingleItem(name)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onFetchItemsStart() }
            .doOnTerminate { onFetchItemsFinish() }
            .subscribe(
                { result -> onFetchItemDetailSuccess(result) }, //Successfully fetched item
                { error -> onError(error) } // An error occurred while fetching item
            )
    }

    private fun getItems() {

        EspressoIdlingResource.increment() // For testing purposes

        subscription = Observable.fromCallable { itemDao.all }
            .concatMap { dbItemList ->
                if (dbItemList.isEmpty())
                    itemApi.getItems().concatMap { itemList ->
                        itemDao.insertAll(*itemList.toTypedArray())
                        Observable.just(itemList)
                    }
                else
                    Observable.just(dbItemList)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onFetchItemsStart() }
            .doOnTerminate { onFetchItemsFinish() }
            .subscribe(
                { result -> onFetchItemListSuccess(result) }, // Successfully fetched item list
                { error -> onError(error) } // An error occurred while fetching item list
            )

    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun onFetchItemDetailSuccess(item: Item) {  // SingleItem
        EspressoIdlingResource.decrement() // For testing purposes
        setLiveDataVariablesForDataBinding(item)
        itemData.value = item;
    }

    /**
     *  Item List for Rv. note that it might be already provided by Room Db.
     *  So this would not be consuming every time remote api data.
     */
    private fun onFetchItemListSuccess(itemsList: List<Item>) {
        EspressoIdlingResource.decrement() // For testing purposes
        listOfItems.value = itemsList;
    }

    private fun onError(error: Throwable) {
        errorMessage.value = error.message
    }

    //Once it starts to fetch item detail to display
    private fun onFetchItemsStart() {
        progressBarVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    //Once it finishes to fetch item detail to display
    private fun onFetchItemsFinish() {
        progressBarVisibility.value = View.GONE
    }

    /**
     *  Following functions are created to demonstrate use of DataBinding in SecondScreen portrait mode.
     *  Whenever singleItem data is received, its properties are stored in liveData variable to make
     *  layout/item_detail.xml to consume them.
     * */

    private fun setLiveDataVariablesForDataBinding(item: Item) {
        itemNameLiveData.value = item.name
        itemTextLiveData.value = item.text
        itemImageUrlLiveData.value = item.image
    }

    fun getItemName(): MutableLiveData<String>? {
        return itemNameLiveData
    }

    fun getItemText(): MutableLiveData<String>? {
        return itemTextLiveData
    }

    fun getItemImageUrl(): MutableLiveData<String>? {
        return itemImageUrlLiveData
    }
}