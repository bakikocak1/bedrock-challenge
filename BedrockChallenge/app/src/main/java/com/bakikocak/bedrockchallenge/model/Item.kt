package com.bakikocak.bedrockchallenge.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Class which provides a model for each Item that will be shown in the app.
 * @constructor Sets all properties of an Item.
 * @property name the unique identifier of an Item
 * @property image the property that holds image url data.
 */
@Entity
data class Item(
    @field:PrimaryKey
    val name: String,
    val text: String?,
    val image: String
)