package com.bakikocak.bedrockchallenge.ui.mainscreen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bakikocak.bedrockchallenge.R
import com.bakikocak.bedrockchallenge.databinding.ItemListInfoLayoutBinding
import com.bakikocak.bedrockchallenge.injection.ItemViewModelFactory
import com.bakikocak.bedrockchallenge.model.Item
import com.bakikocak.bedrockchallenge.viewmodel.ItemViewModel
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.item_list_info_layout.*

/**
 *  The pane on the right side for First Screen (MainActivity)
 * */

class ItemListInfoFragment : Fragment() {

    private lateinit var binding: ItemListInfoLayoutBinding
    private lateinit var itemViewModel: ItemViewModel
    private var itemName: String = ""
    private var errorBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                itemName = it.getString(ARG_ITEM_ID)!! // Receive name property since it is registered as primary key.
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.item_list_info_layout, container, false)


        itemViewModel = ViewModelProvider(this, ItemViewModelFactory(activity as AppCompatActivity, itemName)).get(ItemViewModel::class.java)

        // Observe if there is an error occurring on viewModel and show/hide it.
        itemViewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError() })

        // Observe if there is any change on data set and update UI.
        itemViewModel.itemData.observe(this, Observer {
                item -> if (item != null) populateFields(item) })

        binding.viewModel = itemViewModel

        return binding.root;
    }

    private fun populateFields(item: Item) {
        item_info_text.text = item.text
        item_info_name.text = item.name
        Glide.with(activity as AppCompatActivity)
            .load(item.image.replace("http:", "https:")) // image urls with http changed to https
            .fitCenter()
            .into(item_info_image)
    }

    // Error handling
    private fun showError(errorMessage: String){
        errorBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorBar?.setAction(R.string.retry, itemViewModel.errorClickListener)
        errorBar?.show()
    }

    private fun hideError(){
        errorBar?.dismiss()
    }

    companion object {
        const val ARG_ITEM_ID = "item_id"
    }
}
