package com.bakikocak.bedrockchallenge.injection.component

import com.bakikocak.bedrockchallenge.injection.NetworkModule
import com.bakikocak.bedrockchallenge.viewmodel.ItemListViewModel
import com.bakikocak.bedrockchallenge.viewmodel.ItemViewModel
import dagger.Component

@Component(modules = [NetworkModule::class])
interface ViewModelInjector {

    /**
     * Injects required dependencies into the specified ItemListViewModel.
     * @param itemListViewModel ItemListViewModel in which to inject the dependencies
     */
    fun inject(itemListViewModel: ItemListViewModel)

    /**
     * Injects required dependencies into the specified ItemViewModel.
     * @param itemViewModel ItemViewModel in which to inject the dependencies
     */
    fun inject(itemViewModel: ItemViewModel)


    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }
}