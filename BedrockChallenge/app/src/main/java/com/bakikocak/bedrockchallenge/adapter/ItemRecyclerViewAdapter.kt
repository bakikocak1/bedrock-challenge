package com.bakikocak.bedrockchallenge.adapter

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bakikocak.bedrockchallenge.R
import com.bakikocak.bedrockchallenge.model.Item

import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_list_content.view.*

class ItemRecyclerViewAdapter(
    private val parentActivity: AppCompatActivity,
    val listener: (Item, Int) -> Unit

) :
    RecyclerView.Adapter<ItemRecyclerViewAdapter.ViewHolder>() {

    private var itemsList: List<Item>? = null
    private var selectedPosition = RecyclerView.NO_POSITION
    private var temp = RecyclerView.NO_POSITION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemsList?.get(position)

        holder.itemNameTextView.text = item?.name

        Glide.with(parentActivity)
            .load(item?.image?.replace("http:", "https:")) // image urls with http changed to https
            .fitCenter()
            .into(holder.itemImageView)

        holder.itemView.isSelected = (selectedPosition == position)

        holder.itemView.setOnClickListener { listener(item!!, position) }
    }

    override fun getItemCount() = itemsList?.size ?: 0

    //update data set when it is successfully fetched.
    fun updateItemList(itemList: List<Item>) {
        itemsList = itemList
        notifyDataSetChanged()
    }

    // To keep track of selected row.
    fun setSelectedRowPosition(newPosition: Int) {
        temp = selectedPosition
        selectedPosition = newPosition
        parentActivity.currentFocus?.clearFocus()
        notifyItemChanged(temp)
        notifyItemChanged(selectedPosition)
    }

    // Simple ViewHolder class for RecyclerView
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val itemNameTextView: TextView = view.id_name
        val itemImageView: ImageView = view.id_image
    }
}