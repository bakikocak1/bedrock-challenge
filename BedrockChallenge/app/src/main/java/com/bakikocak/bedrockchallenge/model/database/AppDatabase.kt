package com.bakikocak.bedrockchallenge.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bakikocak.bedrockchallenge.model.Item
import com.bakikocak.bedrockchallenge.model.ItemDao

@Database(entities = [Item::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao
}