package com.bakikocak.bedrockchallenge.network

import com.bakikocak.bedrockchallenge.model.Item
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to fetch items from the webservices.
 */
interface ItemApi {
    /**
     * Get the list of the items from the API
     */
    @GET("/test/json.php")
    fun getItems(): Observable<List<Item>>

    /**
     * Get the list of the items from the API
     */
    @GET("/test/json.php")
    fun getSingleItem(@Query("name") name: String): Observable<Item>
}