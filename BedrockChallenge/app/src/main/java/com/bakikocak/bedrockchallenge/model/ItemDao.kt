package com.bakikocak.bedrockchallenge.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ItemDao {
    @get:Query("SELECT * FROM item")
    val all: List<Item>

    @Insert
    fun insertAll(vararg items: Item)
}