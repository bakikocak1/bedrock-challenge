package com.bakikocak.bedrockchallenge.ui.secondscreen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bakikocak.bedrockchallenge.R
import com.bakikocak.bedrockchallenge.adapter.ItemRecyclerViewAdapter
import com.bakikocak.bedrockchallenge.databinding.ItemDetailBinding
import com.bakikocak.bedrockchallenge.injection.ItemViewModelFactory
import com.bakikocak.bedrockchallenge.model.Item
import com.bakikocak.bedrockchallenge.viewmodel.ItemViewModel
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.item_detail.*

class ItemDetailFragment : Fragment() {
    
    private lateinit var binding: ItemDetailBinding
    private lateinit var itemViewModel: ItemViewModel
    private var itemName: String = ""
    private var adapter: ItemRecyclerViewAdapter? = null
    private var errorBar: Snackbar? = null
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                itemName = it.getString(ARG_ITEM_ID)!! // Receive name property since it is registered as primary key.
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.item_detail, container, false)


        itemViewModel = ViewModelProvider(this, ItemViewModelFactory(activity as AppCompatActivity, itemName)).get(ItemViewModel::class.java)

        // Observe if there is an error occurring on viewModel and show/hide it.
        itemViewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError() })

        // Observe if there is any change on data set and update recyclerView.
        itemViewModel.listOfItems.observe(this, Observer {
                itemList -> if (itemList != null) updateRecyclerView(itemList) })

        // Observe if there is any change on data set and update UI.
        itemViewModel.itemData.observe(this, Observer {
                item -> if (item != null) populateFields(item) })

        binding.viewModel = itemViewModel

        return binding.root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (item_detail_landscape_layout != null) {
            /**
             *  if item_detail_landscape_layout is visible user then twoPane is enabled.
             *  and the recyclerview should be visible. setup recyclerView for two-paned UI.
             * */
            twoPane = true
            setupRecyclerView(item_list)
        }
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        adapter =
            ItemRecyclerViewAdapter(
                activity as AppCompatActivity
            ) { item, position -> updateView(item, position) }
        /**
         *  Note that adapter constructor receives a lambda func. to update UI.
         *  Better way to handle clickListener on adapter then implementing an interface as I was used to do in Java.
         * */

        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = adapter
    }

    // Updates rv once the required list is fetched from ViewModel.
    private fun updateRecyclerView(itemList: List<Item>) {
        adapter?.updateItemList(itemList)
    }

    // Updates second pane UI on adapter click.
    private fun updateView(item: Item, position: Int) {
        adapter?.setSelectedRowPosition(position)
        populateFields(item)
    }

    private fun populateFields(item: Item) {
        /**
         *  For demonstration purposes of how to differentiate screens and usage of databinding and explicitly setting values
         *  let's populate them explicitly if it is two pane UI (Tablet - landscape)
         */

        if (twoPane) {
            item_detail_text.text = item.text
            item_detail_name.text = item.name
            Glide.with(activity as AppCompatActivity)
                .load(item.image.replace("http:", "https:")) // image urls with http changed to https
                .fitCenter()
                .into(item_detail_image)
        }

        /**
         *   -> else these fields will be populated by data binding
         *   please see layout/item_detail.xml and second screen in portrait mode
         * */
    }

    // Error Handling
    private fun showError(errorMessage: String){
        errorBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorBar?.setAction(R.string.retry, itemViewModel.errorClickListener)
        errorBar?.show()
    }

    private fun hideError(){
        errorBar?.dismiss()
    }

    companion object {
        const val ARG_ITEM_ID = "item_id"
    }
}
