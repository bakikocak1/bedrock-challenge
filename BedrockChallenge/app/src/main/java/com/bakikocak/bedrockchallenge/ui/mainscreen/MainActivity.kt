package com.bakikocak.bedrockchallenge.ui.mainscreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bakikocak.bedrockchallenge.R
import com.bakikocak.bedrockchallenge.adapter.ItemRecyclerViewAdapter
import com.bakikocak.bedrockchallenge.databinding.ActivityMainBinding
import com.bakikocak.bedrockchallenge.injection.ItemListViewModelFactory
import com.bakikocak.bedrockchallenge.model.Item
import com.bakikocak.bedrockchallenge.ui.secondscreen.ItemDetailFragment
import com.bakikocak.bedrockchallenge.ui.secondscreen.SecondActivity
import com.bakikocak.bedrockchallenge.viewmodel.ItemListViewModel
import com.google.android.material.snackbar.Snackbar

import kotlinx.android.synthetic.main.item_list.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ItemListViewModel
    private var errorBar: Snackbar? = null
    private var adapter: ItemRecyclerViewAdapter? = null
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        // View Model
        viewModel = ViewModelProvider(
            this,
            ItemListViewModelFactory(this)
        ).get(ItemListViewModel::class.java)

        // Observe if there is any change on data set and update recyclerView.
        viewModel.listOfItems.observe(this, Observer {
                itemList -> if (itemList != null) updateRecyclerView(itemList) })

        // Observe if there is an error occurring on viewModel and show/hide it.
        viewModel.errorMessage.observe(
            this,
            Observer { errorMessage -> if (errorMessage != null) showError(errorMessage) else hideError() })

        binding.viewModel = viewModel

        // if second pane is visible to user
        if (item_info_container != null) {
            twoPane = true
        }

        setupRecyclerView(item_list)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        adapter =
            ItemRecyclerViewAdapter(
                this
            ) { item, position -> updateView(item, position) }
        // Note that adapter constructor receives a lambda func. to update UI

        recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = adapter
    }

    private fun updateView(item: Item, position: Int) {
        // Track selected row and notify adapter
        adapter?.setSelectedRowPosition(position)

        // Transition depending on if it is a mobile phone or tablet.
        if (twoPane) {
            /**
             * If it is run on tablet, an instance of ItemListInfoFragment
             * is added to item_info_container which holds second pane UI.
             */

            val fragment = ItemListInfoFragment()
                .apply {
                    arguments = Bundle().apply {
                        putString(ItemListInfoFragment.ARG_ITEM_ID, item.name)
                    }
                }
            this.supportFragmentManager
                .beginTransaction()
                .replace(R.id.item_info_container, fragment)
                .commit()
        } else {
            /**
             * If it is a mobile device this will create an Intent to
             *  pass item.name (primary key) data to SecondActivity.
             */

            val intent = Intent(this, SecondActivity::class.java).apply {
                putExtra(ItemDetailFragment.ARG_ITEM_ID, item.name)
            }
            this.startActivity(intent)
        }
    }

    // Updates rv once the required list is fetched from ViewModel.

    private fun updateRecyclerView(itemList: List<Item>) {
        adapter?.updateItemList(itemList)
    }

    // Error handling: Note that error message is also observed from ViewModel
    private fun showError(errorMessage: String) {
        errorBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorBar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorBar?.show()
    }

    private fun hideError(){
        errorBar?.dismiss()
    }
}
