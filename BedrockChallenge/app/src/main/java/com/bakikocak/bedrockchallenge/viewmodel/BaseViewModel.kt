package com.bakikocak.bedrockchallenge.viewmodel

import androidx.lifecycle.ViewModel
import com.bakikocak.bedrockchallenge.injection.NetworkModule
import com.bakikocak.bedrockchallenge.injection.component.DaggerViewModelInjector
import com.bakikocak.bedrockchallenge.injection.component.ViewModelInjector

abstract class BaseViewModel: ViewModel(){
    // BaseViewModel which will be used for Dependency Injection.
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is ItemListViewModel -> injector.inject(this)
            is ItemViewModel -> injector.inject(this)
        }
    }
}