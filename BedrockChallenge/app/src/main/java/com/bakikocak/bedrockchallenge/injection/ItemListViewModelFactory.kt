package com.bakikocak.bedrockchallenge.injection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.bakikocak.bedrockchallenge.model.database.AppDatabase
import com.bakikocak.bedrockchallenge.viewmodel.ItemListViewModel

class ItemListViewModelFactory(
    private val activity: AppCompatActivity
): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemListViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "items").build()
            return ItemListViewModel(db.itemDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}