package com.bakikocak.bedrockchallenge.viewmodel


import android.view.View
import androidx.lifecycle.MutableLiveData
import com.bakikocak.bedrockchallenge.model.Item
import com.bakikocak.bedrockchallenge.model.ItemDao
import com.bakikocak.bedrockchallenge.network.ItemApi
import com.bakikocak.bedrockchallenge.utils.EspressoIdlingResource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ItemListViewModel(private val itemDao: ItemDao) : BaseViewModel() {
    // The instance of Item Api that will be injected by Dagger for retrieving data.
    @Inject
    lateinit var itemApi: ItemApi

    private lateinit var subscription: Disposable

    var listOfItems: MutableLiveData<List<Item>> =
        MutableLiveData() // will be observed by MainActivity
    val progressBarVisibility: MutableLiveData<Int> =
        MutableLiveData() // will be observed by MainActivity
    val errorMessage: MutableLiveData<String> =
        MutableLiveData() // will be observed by MainActivity
    val errorClickListener = View.OnClickListener { getItems() }

    init {
        getItems()
    }

    private fun getItems() {

        EspressoIdlingResource.increment() // For testing purposes

        subscription = Observable.fromCallable { itemDao.all }
            .concatMap { dbItemList ->
                if (dbItemList.isEmpty())
                    itemApi.getItems().concatMap { itemList ->
                        itemDao.insertAll(*itemList.toTypedArray())
                        Observable.just(itemList)
                    }
                else
                    Observable.just(dbItemList)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onFetchItemsStart() }
            .doOnTerminate { onFetchItemsFinish() }
            .subscribe(
                { result -> onFetchItemsSuccess(result) }, // Successfully fetched item list
                { error -> onFetchItemsError(error) } // An error occurred while fetching item list
            )

    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun onFetchItemsSuccess(itemList: List<Item>) {
        EspressoIdlingResource.decrement() // For testing purposes
        listOfItems.value = itemList
    }

    private fun onFetchItemsError(error: Throwable) {
        errorMessage.value = error.message
    }

    //Once it starts to fetch items to display
    private fun onFetchItemsStart() {
        progressBarVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    //Once it finishes to fetch items to display
    private fun onFetchItemsFinish() {
        progressBarVisibility.value = View.GONE
    }
}