package com.bakikocak.bedrockchallenge.injection

import com.bakikocak.bedrockchallenge.network.ItemApi
import com.bakikocak.bedrockchallenge.utils.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Module to inject required dependencies about networking
 * (ex: Retrofit interface, Api to fetch "Items")
 */

@Module
@Suppress("unused")
object NetworkModule {
    /**
     * Provides the Item service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Item API service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideItemApi(retrofit: Retrofit): ItemApi {
        return retrofit.create(ItemApi::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}