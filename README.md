![alt text](https://media-exp1.licdn.com/dms/image/C4E0BAQF39rFgTU0bPA/company-logo_200_200/0?e=1599696000&v=beta&t=rgp6Xz2WNqrTwUULDfR1_P_topqFzL_k3OVZfUYA3OI)

# Bedrock Challenge


This is a mini MVVM Android project that fetches data from api and shows it to the user in different forms depending on
devices screen size and orientation.

The general architecture is design based on Android Architecture Components suggested by Google.


![alt text](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)

**Some points to highlight:**

1.  To store data, Entity and Data Access Object(DAO) created.
2.  Item List is fetched from API by using Retrofit and data save in Room database. Thanks to Room, even if the connection is gone, Recyclerviews will be populated with latest version of the data fetched from the server.
    * Note that single item data is not saved in Room database to be able to test retry loading when connections is established again.
3. ViewModel acts as a communication center between the api and the UI. Hides where the data originates from the UI. ViewModel instances survive configuration changes.
	* Note that a repository is not designed and written for simplicity of the project.
4. Data fetched in ViewModels is observed by UI (Acitivity/Fragment).



**Technical Details:**

1. Kotlin
2. Android Studio / Gradle
3. MVVM App Arch Components (LiveData, Room, ViewModel)
4. RxAndroid
5. Dagger2
6. Glide

**Test cases / Assumptions: **

** - While data for recyclerviews (@GET("/test/json.php")) is saved in Room database that keeps the latest version of fetched data, 
  data for single items are not saved just to show that it is possible send another request to fetch data.
  To test it: **
  
  
  * Launch app in portrait orientation and see that recyclerview is populated with the data.
  * Exit the app and get the device in flight mode. (without any connection)
  * Launch the app, data for recyclerview is loaded from database.
  * Click on a row to go to second screen and see the error message corresponding to connection issue.
  * Reconnect your device to the internet and tap Retry on Snackbar.
  * See that app loads the data.
  
  
** - In the second screen portrait mode image, name, text data is binded from ItemViewModel via DataBinding using BindingAdapters class. 
     Note that some classes use databinding while others do not. It is not because of any architecture problem 
     but it is more to demonstrate several ways to use data and populate UI. To notice this differentiation: **
      
  * Launch app in portrait orientation and see that recyclerview is populated with the data.
  * Note that this data is populated by an adapter which does not use databinding even if mainActivity - viewModel binding is established.
  * Go to second screen in portrait mode and see that data is binded in layout/item_detail.xml
  * This xml gets livedata as MutableLiveData from ItemViewModel (more info: line 123 in ItemViewModel)

** - Some basic UI test cases by Espresso are also added as bonus. 
   I thought it might be possible to manage screen rotations by Espresso but seems like it is not possible :) **
   
** Retrofit Api is injected into both viewModels by Dagger 2 via NetworkModule as module and ViewModelInjector as component. **

Latest version of the code is available on "master" branch.
   

